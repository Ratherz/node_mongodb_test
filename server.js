const express = require('express');
const bodyParser = require('body-parser');
const db = require('./Databases');
const Product = require('./Models/Product');
const cors = require('cors');
const allowedOrigins = [
    'http://localhost:4200',
];
const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Reflect the origin if it's in the allowed list or not defined (cURL, Postman, etc.)
const corsOptions = {
    origin: (origin, callback) => {
        if (allowedOrigins.includes(origin) || !origin) {
            callback(null, true);
        } else {
            callback(new Error('Origin not allowed by CORS'));
        }
    },
};

// Enable preflight requests for all routes
app.options('*', cors(corsOptions));

app.use(function (req, res, next) {
    res.header(
        "Access-Control-Allow-Origin",
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
    );
    next();
});

app.get('/', (_req, res) => {
    res.status(200).send("Hello world");
});

app.post('/products', cors(corsOptions), async (req, res) => {
    const payload = req.body;
    const product = new Product(payload);
    await product.save();
    res.status(201).json({ status: 200, message: "Success!" });
});

app.get('/products', cors(corsOptions), async (_req, res) => {
    const products = await Product.find({});
    res.status(200).json({ status: 200, results: products });
});

app.get('/products/:id', cors(corsOptions), async (req, res) => {
    const { id } = req.params;
    const product = await Product.findById(id);
    res.status(200).json({ status: 200, result: product });
});

app.put('/products/:id', cors(corsOptions), async (req, res) => {
    const payload = req.body;
    const { id } = req.params;
    await Product.findByIdAndUpdate(id, { $set: payload });
    res.status(200).json({ status: 200, message: "Update Success!" });
});

app.delete('/products/:id', cors(corsOptions), async (req, res) => {
    const { id } = req.params;
    await Product.findByIdAndDelete(id);
    res.status(204).json({ status: 204, message: 'Delete Success!' });
});

app.listen(8080, () => console.log('listening on port: 8080'));